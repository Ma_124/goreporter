package report

import (
	"testing"
	"html"
	"bytes"
	"github.com/sergi/go-diff/diffmatchpatch"
	"gopkg.in/src-d/go-git.v4/utils/ioutil"
	ioutil2 "io/ioutil"
	"os"
)

func TestMarkdownReporter_Escape(t *testing.T) {
	esced := `&lt;b&gt; &amp;amp; &lt;/b&gt;`
	raw := `<b> &amp; </b>`

	if esced != html.EscapeString(raw) {
		t.Fatal("\n" + html.EscapeString(raw) + " !=\n" + esced)
	}
}

func TestMarkdownReporter_WriteHeader(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteHeader("Header", 1)
		r.WriteLine("Text")
	}, `
Header
======

Text
`)

	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteHeader("Header 2", 2)
		r.WriteHeader("Header 3", 3)
	}, `
Header 2
--------

### Header 3
`)
}

func TestMarkdownReporter_WriteBlockQuote(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteBlockQuote("Hello World>&&\n -literally every programmer")
	}, `
> Hello World&gt;&amp;&amp;
>  -literally every programmer
`)
}

func TestMarkdownReporter_StartOrderedList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.StartOrderedList()
		r.WriteOrderedListItem("Item 0")
		r.WriteOrderedListItem("Item 1")
		r.EndOrderedList()
	}, `
1. Item 0
2. Item 1

`)
}

func TestMarkdownReporter_WriteOrderedList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteOrderedList([]string{"Slice[0]", "Slice[1]", "Slice[2]"})
	}, `
1. Slice[0]
2. Slice[1]
3. Slice[2]

`)
}

func TestMarkdownReporter_StartList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.StartList()
		r.WriteListItem("An item")
		r.WriteListItem("another item")
		r.WriteListItem("just another one")
		r.EndList()
	}, `
* An item
* another item
* just another one

`)
}

func TestMarkdownReporter_WriteList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteList([]string{"item", "just an item", "boooring"})
	}, `
* item
* just an item
* boooring

`)
}

func TestMarkdownReporter_StartTaskList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.StartTaskList()
		r.WriteTaskListItem("Lettuce", true)
		r.WriteTaskListItem("Bread", false)
		r.WriteTaskListItem("Rice", true)
		r.EndTaskList()
	}, `
-[x] Lettuce
-[ ] Bread
-[x] Rice

`)
}

func TestMarkdownReporter_WriteTaskList(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteTaskList([]string{"Fix #5726", "Test Coverage"})
	}, `
-[ ] Fix #5726
-[ ] Test Coverage

`)
}

func TestMarkdownReporter_WriteHorizontalRule(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteHorizontalRule()
	}, `
* * * * * * * * * * * *

`)
}

func TestMarkdownReporter_WriteLink(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteLink("Example", "http://example.com")
	}, `
[Example](http://example.com "Example")`)
}

func TestMarkdownReporter_WriteLinkWTitle(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteLinkWTitle("Example", "http://example.org", "This domain is established to be used for illustrative examples in documents.")
	}, `
[Example](http://example.org "This domain is established to be used for illustrative examples in documents.")`)
}

func TestMarkdownReporter_WriteMedia(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteMedia("Java BBQ", "https://ma124.js.org/java_bbq.jpg")
	}, `
![Java BBQ](https://ma124.js.org/java_bbq.jpg "Java BBQ")`)
}

func TestMarkdownReporter_WriteMediaWTitle(t *testing.T) {
	testMarkdownReporter(t, func(r *MarkdownReporter) {
		r.WriteMediaWTitle("Java BBQ", "https://ma124.js.org/java_bbq.jpg", "an old one")
	}, `
![Java BBQ](https://ma124.js.org/java_bbq.jpg "an old one")`)
}

func testMarkdownReporter(t *testing.T, do func(r *MarkdownReporter), expected string) {
	buf := new(bytes.Buffer)
	r := NewMarkdownReporter(ioutil.NewWriteCloser(buf, nil))
	r.WriteLine("")

	do(r)

	if buf.String() != expected {
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(expected, buf.String(), false)
		ioutil2.WriteFile("diff.html", []byte("<style>del { background-color: red !important; } ins { background-color: #41B62A !important; }</style><pre>" + dmp.DiffPrettyHtml(diffs) + "</pre>"), os.ModePerm)
		t.Fatalf(dmp.DiffPrettyText(diffs))
	}
}
