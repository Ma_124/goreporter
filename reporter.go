package report

import "io"

type Reporter interface {
	io.WriteCloser

	WriteRaw(s string) error
	WriteText(s string) error
	WriteLine(s string) error
	Escape(s string) string

	WriteHeader(s string, lvl int) error
	WriteBlockQuote(s string) error

	StartOrderedList() error
	EndOrderedList() error
	WriteOrderedListItem(s string) error
	WriteOrderedList(s []string) error

	StartList() error
	EndList() error
	WriteListItem(s string) error
	WriteList(s []string) error

	StartTaskList() error
	EndTaskList() error
	WriteTaskListItem(s string, selected bool) error
	WriteTaskList(s []string) error

	WriteInlineCode(s string, lang string) error
	WriteCode(s string, lang string) error

	WriteHorizontalRule() error

	WriteLink(s string, url string) error
	WriteMedia(s string, url string) error

	StartTable() error
	EndTable() error
	WriteTableHeader(s []string) error
	StartTableRow() error
	EndTableRow() error
	WriteTableRow(s []string) error
	WriteTableElement(s string) error
}
