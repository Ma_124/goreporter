package report

import (
	"io"
	"html"
	"errors"
	"strings"
	"strconv"
)

type MarkdownReporter struct {
	w io.WriteCloser

	AtxOnly bool

	oli int
}

func NewMarkdownReporter(w io.WriteCloser) *MarkdownReporter {
	return &MarkdownReporter{
		w: w,
		AtxOnly: false,
	}
}

func (r *MarkdownReporter) WriteRaw(s string) error {
	_, err := r.Write([]byte(s))
	return err
}

func (r *MarkdownReporter) WriteText(s string) error {
	return r.WriteRaw(r.Escape(s))
}

func (r *MarkdownReporter) WriteLine(s string) error {
	return r.WriteText(s + "\n")
}

func (r *MarkdownReporter) Escape(s string) string {
	return html.EscapeString(s)
}

func (r *MarkdownReporter) repeat(rr rune, no int) string {
	arr := make([]rune, no)

	for i := 0; i < no; i++ {
		arr[i] = rr
	}

	return string(arr)
}

func (r *MarkdownReporter) repeatToLen(s string, leng int) {
	arr := make([]byte, leng)

	j := 0

	for i := 0; i < leng; i++ {
		arr[i] = s[j]
		j++
		if j == leng {
			j = 0
		}
	}
}

func (r *MarkdownReporter) WriteHeader(s string, lvl int) error {
	if lvl < 1 {
		return errors.New("lvl < 1")
	}
	if !r.AtxOnly {
		switch lvl {
		case 1:
			return r.WriteSetextHeader(s, '=')
		case 2:
			return r.WriteSetextHeader(s, '-')
		}
		// fallthrough
	}

	return r.WriteAtxHeader(s, lvl)
}

func (r *MarkdownReporter) WriteSetextHeader(s string, ul rune) error {
	err := r.WriteText(s)
	if err != nil {
		return err
	}
	return r.WriteText("\n" + r.repeat(ul, len(s)) + "\n\n")
}

func (r *MarkdownReporter) WriteAtxHeader(s string, lvl int) error {
	err := r.WriteText(r.repeat('#', lvl))
	if err != nil {
		return err
	}
	return r.WriteText(" " + s + "\n")
}

func (r *MarkdownReporter) WriteBlockQuote(s string) error {
	return r.WriteRaw("> " + strings.Replace(r.Escape(strings.Trim(s, "\n\r")), "\n", "\n> ", -1) + "\n")
}

func (r *MarkdownReporter) StartOrderedList() error {
	r.oli = 0
	return nil
}

func (r *MarkdownReporter) EndOrderedList() error {
	return r.WriteLine("")
}

func (r *MarkdownReporter) WriteOrderedListItem(s string) error {
	r.oli++
	return r.WriteRaw(strconv.Itoa(r.oli) + ". " + r.Escape(s) + "\n")
}

func (r *MarkdownReporter) WriteOrderedList(s []string) error {
	err := r.StartOrderedList()
	if err != nil {
		return err
	}
	for _, it := range s {
		err = r.WriteOrderedListItem(it)
		if err != nil {
			return err
		}
	}
	return r.EndOrderedList()
}

func (r *MarkdownReporter) StartList() error {
	return nil
}

func (r *MarkdownReporter) EndList() error {
	return r.WriteLine("")
}

func (r *MarkdownReporter) WriteListItem(s string) error {
	return r.WriteRaw("* " + r.Escape(s) + "\n")
}

func (r *MarkdownReporter) WriteList(s []string) error {
	err := r.StartList()
	if err != nil {
		return err
	}
	for _, it := range s {
		err = r.WriteListItem(it)
		if err != nil {
			return err
		}
	}
	return r.EndList()
}

func (r *MarkdownReporter) StartTaskList() error {
	return nil
}

func (r *MarkdownReporter) EndTaskList() error {
	return r.WriteLine("")
}

func (r *MarkdownReporter) WriteTaskListItem(s string, selected bool) error {
	if selected {
		return r.WriteRaw("-[x] " + r.Escape(s) + "\n")
	}
	return r.WriteRaw("-[ ] " + r.Escape(s) + "\n")
}

func (r *MarkdownReporter) WriteTaskList(s []string) error {
	err := r.StartTaskList()
	if err != nil {
		return err
	}
	for _, it := range s {
		err = r.WriteTaskListItem( it, false)
		if err != nil {
			return err
		}
	}
	return r.EndTaskList()
}

func (r *MarkdownReporter) WriteInlineCode(s string, lang string) error {
	return r.WriteRaw("`" + r.Escape(s) + "`")
}

func (r *MarkdownReporter) WriteCode(s string, lang string) error {
	return r.WriteRaw("```" + lang + "\n" + r.Escape(s) + "\n```\n\n")
}

func (r *MarkdownReporter) WriteHorizontalRule() error {
	return r.WriteRaw("* * * * * * * * * * * *\n\n")
}

func (r *MarkdownReporter) WriteLinkWTitle(s string, url string, title string) error {
	return r.WriteRaw("[" + r.Escape(s) + "](" + url + " \"" + title + "\")")
}

func (r *MarkdownReporter) WriteLink(alt string, url string) error {
	return r.WriteLinkWTitle(alt, url, alt)
}

func (r *MarkdownReporter) WriteMediaWTitle(alt string, url string, title string) error {
	return r.WriteRaw("![" + r.Escape(alt) + "](" + url + " \"" + title + "\")")
}

func (r *MarkdownReporter) WriteMedia(s string, url string) error {
	return r.WriteMediaWTitle(s, url, s)
}

func (r *MarkdownReporter) StartTable() error {
	return nil
}

func (r *MarkdownReporter) EndTable() error {
	panic("implement me")
}

func (r *MarkdownReporter) WriteTableHeader(s []string) error {
	panic("implement me")
}

func (r *MarkdownReporter) StartTableRow() error {
	panic("implement me")
}

func (r *MarkdownReporter) EndTableRow() error {
	panic("implement me")
}

func (r *MarkdownReporter) WriteTableRow(s []string) error {
	panic("implement me")
}

func (r *MarkdownReporter) WriteTableElement(s string) error {
	panic("implement me")
}

func (r *MarkdownReporter) Write(p []byte) (n int, err error) {
	return r.w.Write(p)
}

func (r *MarkdownReporter) Close() error {
	return r.w.Close()
}
